package com.example.securecontacts

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.securecontacts.database.ContactsDatabase
import com.example.securecontacts.models.Contact

class CustomAdapter(
    context: Context,
    private val contacts: List<Contact>,
    val database: ContactsDatabase?
) : BaseAdapter() {
    private val mContext: Context

    //Artur: po co jest ten init? z tym też działa: private val mContext:Context=context
    init {
        mContext = context
    }

    override fun getCount(): Int {
        return contacts.size
    }

    override fun getItem(position: Int): Any {
        return "TEST STRING"
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(mContext)
        var rowItem = layoutInflater.inflate(R.layout.row_item, parent, false)
        rowItem.findViewById<ImageView>(R.id.imageViewCall).setOnClickListener {
            contacts[position].isDeleted = !contacts[position].isDeleted



            Thread {
                database?.contactsDao()?.update(contacts[position])
            }.start()
            notifyDataSetChanged()
        }
        rowItem.findViewById<CheckBox>(R.id.doubleChecked).setOnClickListener { arg0 ->
            val isChecked = (arg0 as CheckBox).isChecked
            if (isChecked) {
                println("yes")
                println(contacts[position].toString())
                contacts[position].doubleChecked = true
                Thread {
                    database?.contactsDao()?.update(contacts[position])
                }.start()
            } else {
                println("no")
                contacts[position].doubleChecked = false
                Thread {
                    database?.contactsDao()?.update(contacts[position])
                }.start()
            }
        }
        val deleteButton = rowItem.findViewById(R.id.deleted_flag) as Button
       // deleteButton.tag = position

        deleteButton.setOnClickListener {
          //  val index = rowItem.getTag() as Int
            deleteButton.text="usuń"
 //notifyDataSetChanged()
            Thread {
                database?.contactsDao()!!.findAllDeleted()


            }.start()
            Log.e("delete_button","delete_button")
            notifyDataSetChanged()
        }
//            var phon_no=rowItem!!.findViewById<TextView>(R.id.textViewNumber)
//            val phone = "tel:" +phon_no.text
//            val i = Intent(Intent.ACTION_DIAL, Uri.parse(phone))
//           startActivity(mContext,i,null)
        rowItem.findViewById<TextView>(R.id.textViewName).text = contacts[position].name
        if (contacts[position].isDeleted) {
            rowItem.findViewById<TextView>(R.id.textViewNumber).text = "DELETED"
        } else {
            rowItem.findViewById<TextView>(R.id.textViewNumber).text = contacts[position].number
        }
        rowItem.findViewById<CheckBox>(R.id.doubleChecked).isChecked =
            contacts[position].doubleChecked



        return rowItem
    }
}