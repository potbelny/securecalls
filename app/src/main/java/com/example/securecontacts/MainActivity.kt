package com.example.securecontacts

import android.Manifest
import android.app.AlertDialog
import android.app.ProgressDialog.show
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.database.Cursor
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat
import androidx.room.Room
import com.example.securecontacts.database.ContactsDatabase
import com.example.securecontacts.models.Contact


class MainActivity : AppCompatActivity() {



    ////////////////

    private var showOnlyDeleted: Boolean = false
    private var showOnlyDoubleChecked=false
    val PERMISSIONS_REQUEST_READ_CONTACTS = 1
    var listView: ListView? = null
    var database: ContactsDatabase? = null
    var database_contacts: ContactsDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
         listView =findViewById<ListView>(R.id.listView)

        database = Room.databaseBuilder(
            applicationContext,
            ContactsDatabase::class.java, "database-name_3"
        )
            .build()
        requestContactPermission()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.user_menu, menu)
        return true
    }

    fun switchOnlyDeleted() {
        showOnlyDeleted = !showOnlyDeleted
        if (showOnlyDeleted) {
            Thread {
                val databaseContacts=database?.contactsDao()?.findAllDeleted() ?: emptyList() //database?.contactsDao()?.getAll() ?: emptyList()
                runOnUiThread { listView!!.setAdapter(
                    CustomAdapter(this, databaseContacts, database)
                ) }
            }.start()
        } else {
            Thread {
                val databaseContacts=database?.contactsDao()?.findAllNotDeleted() ?: emptyList()
                runOnUiThread { listView!!.setAdapter(
                    CustomAdapter(this, databaseContacts, database)
                ) }
            }.start()
        }
    }

    fun switchOnlyDoubleChecked() {
        showOnlyDoubleChecked = !showOnlyDoubleChecked
        if (showOnlyDoubleChecked) {
            Thread {
                val databaseContacts=database?.contactsDao()?.findAllDdoubleChecked() ?: emptyList() //database?.contactsDao()?.getAll() ?: emptyList()
                runOnUiThread { listView!!.setAdapter(
                    CustomAdapter(this, databaseContacts, database)
                ) }
            }.start()
        } else {
            Thread {
                val databaseContacts=database?.contactsDao()?.findAllNotDoubleChecked() ?: emptyList()
                runOnUiThread { listView!!.setAdapter(
                    CustomAdapter(this, databaseContacts, database)
                ) }
            }.start()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.getItemId()) {
        // action with ID action_refresh was selected
        R.id.logout_menu ->
            switchOnlyDeleted()
        R.id.doubleCheck -> switchOnlyDoubleChecked()
        //Artur: jeśli byśmy chcieli zrobić poniższy zapis, to gdzie obsługujemy i w jaki sposób onOptionsItemSelected(item)?
        //  else ->super.onOptionsItemSelected(item)
    }
        return true;
}

    private fun read() {
        var cursor: Cursor? =contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null,
            null,
            null,
            null
        )
        startManagingCursor(cursor)
        var from=arrayOf(
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.CommonDataKinds.Phone._ID
        )
        var to= intArrayOf(android.R.id.text1, android.R.id.text2)

        var simple:SimpleCursorAdapter=
            SimpleCursorAdapter(
                this,
                android.R.layout.simple_expandable_list_item_2,
                cursor,
                from,
                to
            )
        listView!!.adapter=simple

    }
    fun requestContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_CONTACTS
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        Manifest.permission.READ_CONTACTS
                    )
                ) {
                    val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                    builder.setTitle("Read Contacts permission")
                    builder.setPositiveButton(android.R.string.ok, null)
                    builder.setMessage("Please enable access to contacts.")
                    builder.setOnDismissListener(DialogInterface.OnDismissListener {
                        requestPermissions(
                            arrayOf(Manifest.permission.READ_CONTACTS),
                            PERMISSIONS_REQUEST_READ_CONTACTS
                        )
                    })
                    builder.show()
                } else {
                    requestPermissions(
                        this, arrayOf(Manifest.permission.READ_CONTACTS),
                        PERMISSIONS_REQUEST_READ_CONTACTS
                    )
                }
            } else {
                //read()
                populate()
            }
        } else {
            //read()
            populate()
        }
    }

    fun populate(){
        val cursorCustomList = contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
            null, null
        )
        val contacts: MutableList<Contact> = mutableListOf<Contact>()

        Log.d("rows", Integer.toString(cursorCustomList!!.count))

        Log.d("phone", "here")
        while (cursorCustomList!!.moveToNext()) {
            val phoneNumber = cursorCustomList!!.getString(
                cursorCustomList
                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
            )
            val name = cursorCustomList!!.getString(
                cursorCustomList
                    .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)
            )
            Log.i("Number + Name", phoneNumber + name)
        }
        if (cursorCustomList!!.count > 0) {
            var i = 0
            if (cursorCustomList!!.moveToFirst()) {
                do {
                    val phoneNumber = cursorCustomList
                        .getString(
                            cursorCustomList
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                        )
                    val contact_name = cursorCustomList
                        .getString(
                            cursorCustomList
                                .getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)
                        )
                    contacts.add(i, Contact(i, contact_name, phoneNumber, false,false))
                    i++
                } while (cursorCustomList!!.moveToNext())

                Thread {
                    


                    database?.contactsDao()?.insertList(contacts)
                    val databaseContacts=database?.contactsDao()?.findAllNotDeleted() ?: emptyList()
                    runOnUiThread { listView!!.setAdapter(
                        CustomAdapter(
                            this,
                            databaseContacts,
                            database
                        )
                    ) }

                }.start()


            }
        }
        listView!!.setAdapter(CustomAdapter(this,contacts,database))
        cursorCustomList!!.close()




    }


}