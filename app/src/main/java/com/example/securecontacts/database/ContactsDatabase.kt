package com.example.securecontacts.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.securecontacts.DAO.ContactsDAO
import com.example.securecontacts.models.Contact

@Database(entities = arrayOf(Contact::class), version = 2)
abstract class ContactsDatabase : RoomDatabase() {
    //Artur: gdzie jest ciało metody: contactsDao()?
    abstract fun contactsDao(): ContactsDAO
}