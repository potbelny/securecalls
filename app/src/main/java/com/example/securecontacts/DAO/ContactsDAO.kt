package com.example.securecontacts.DAO

import androidx.room.*
import com.example.securecontacts.models.Contact

@Dao
    interface ContactsDAO {
        @Query("SELECT * FROM contact")
        fun getAll(): List<Contact>

        @Query("SELECT * FROM contact WHERE uid IN (:contactIds)")
        fun loadAllByIds(contactIds: IntArray): List<Contact>

        @Query(
            "SELECT * FROM contact WHERE name LIKE :name AND " +
                    "number LIKE :number LIMIT 1"
        )
        fun findByName(name: String, number: String): Contact

    @Query(
        "SELECT * FROM contact WHERE isDeleted = 1"
    )
    fun findAllDeleted(): List<Contact>

    @Query(
        "SELECT * FROM contact WHERE isDeleted = 0"
    )
    fun findAllNotDeleted(): List<Contact>

    @Query(
        "SELECT * FROM contact WHERE doubleChecked = 1"
    )
    fun findAllDdoubleChecked(): List<Contact>

    @Query(
        "SELECT * FROM contact WHERE doubleChecked = 0"
    )
    fun findAllNotDoubleChecked(): List<Contact>

        @Insert
        fun insertAll(vararg users: Contact)

        @Insert(onConflict = OnConflictStrategy.IGNORE)
        @JvmSuppressWildcards
        fun insertList(objects: List<Contact>)

        @Delete
        fun delete(user: Contact)

    @Query("DELETE FROM contact WHERE uid = :id")
    fun deleteById(id: Int)

        @Update
        fun update(user: Contact)

        @Query("SELECT COUNT(*) FROM contact")
        fun getDataCount(): Int
        }