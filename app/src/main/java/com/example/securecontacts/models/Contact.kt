package com.example.securecontacts.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Contact(
    @PrimaryKey val uid: Int,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "number") val number: String?,
    @ColumnInfo(name = "isDeleted") var isDeleted: Boolean = false,
    @ColumnInfo(name = "doubleChecked") var doubleChecked: Boolean = false
)